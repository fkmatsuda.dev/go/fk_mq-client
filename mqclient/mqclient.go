package mqclient

import (
	"log"

	"gitlab.com/fkmatsuda.dev/go/fk_mq-client/mqclient/mqmessage"

	"net/url"

	"github.com/gorilla/websocket"
)

// MQClient is a client for the fk_mq server
type MQClient struct {
	c *websocket.Conn
	u *url.URL
}

// Connect connects to the fk_mq server
func Connect(u *url.URL) (*MQClient, error) {
	sUrl := u.String()
	log.Default().Printf("Connecting to %s\n", sUrl)
	c, _, err := websocket.DefaultDialer.Dial(sUrl, nil)
	if err != nil {
		return nil, err
	}
	return &MQClient{c: c, u: u}, nil
}

// Write writes a message to the fk_mq server
func (c *MQClient) Write(m *mqmessage.Message) error {
	return c.c.WriteJSON(m)
}

func (c *MQClient) read() (*mqmessage.Message, error) {
	var m mqmessage.Message
	err := c.c.ReadJSON(&m)
	return &m, err
}

// OnMessage registers a callback function to be called when a message is received
func (c *MQClient) OnMessage(listener func(m *mqmessage.Message, err error)) {
	go func() {
		for {
			m, err := c.read()
			if err != nil {
				listener(nil, err)
				return
			}
			listener(m, nil)
		}
	}()
}

// Close closes the connection to the fk_mq server
func (c *MQClient) Close() {
	c.c.Close()
}
