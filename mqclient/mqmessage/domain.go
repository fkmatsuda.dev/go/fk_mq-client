package mqmessage

import "github.com/google/uuid"

// Msg - message data
type Message struct {

	// To - the destinatary
	To uuid.UUID `json:"to,omitempty"`

	// Tag - simple tag to generic use
	Tag string `json:"tag,omitempty"`

	// Payload - the message
	Payload string `json:"payload,omitempty"`
}
