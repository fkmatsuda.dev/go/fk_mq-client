package mqclient

import (
	"log"
	"net/url"

	"gitlab.com/fkmatsuda.dev/go/fk_mq-client/mqclient/mqmessage"
)

func Observe(URL *url.URL) {
	c, err := Connect(URL)
	if err != nil {
		panic(err)
	}
	msgChan := make(chan *mqmessage.Message, 100)
	c.OnMessage(func(m *mqmessage.Message, err error) {
		msgChan <- m
	})

	for {
		select {
		case msg := <-msgChan:
			log.Default().Printf("[%s] %s\n", msg.Tag, msg.Payload)
		}
	}

}
